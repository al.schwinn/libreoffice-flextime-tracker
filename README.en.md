**Libreoffice Table for simple management of come/leave times for GSI/FAIR**

*Read this in other languages: [English](README.en.md), [Deutsch](README.md)*

- Easily customizable
- Support for part-time models
- Bash scripts for automatic tracking
- Monthly summary that can be copied 1:1 into the GSI time tracking form (Vaccation/Illness/etc. are not explicitly listed, though thats no problem for 'PA')

![screenshot](./static/screenshot.png)

**Quick Start Guide**

- Open the 'general' tab

![general](./static/general.png)

- Enter the first day of the current year in A2 (First day of year)
- Enter the carryover flextime from last year in B4 (Credit from last year [h])
- Enter the vacation left from last year in B5 (Vacation left from last year)
- Enter the new vacation for this year in B6 (New vacation for this year)
- Enter the new company leisure time for this year in B10 (betrFZ for this year)
- Optional: If you are working part-time, enter the corresponding weekly hours in B13-B17

Done. Now, on the first working day in January, you can enter your start time/end time.

**Scripts for automatic time entry**

Under Linux, the `on_login` and `on_logout` scripts can be used to automatically track working hours upon login/logout on the computer (with optional +/- 5 minutes extra). How to execute a script on login/logout depends on the desktop environment being used.

**Movable holidays**

Movable holidays need to be entered manually. See also: [Infos Heinerfest and Fastnachtsdienstag](https://www.gsi.de/work/administration/personalabteilung/arbeitszeitinformationen)
