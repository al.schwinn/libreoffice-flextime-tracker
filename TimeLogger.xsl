<?xml version="1.0" encoding="ISO-8859-1"?>


<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
xmlns:calcext="urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0">
<xsl:output method="xml"/>
<xsl:output indent="yes"/>
    
<xsl:param name="dateString"/>
<xsl:param name="isArrival"/>
<xsl:param name="time_h"/>
<xsl:param name="time_m"/>

<xsl:variable name="test" select="$dateString"/>

<!-- This is an identity template - it copies everything that doesn't match another template -->
<xsl:template match="@* | node()">
    <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
</xsl:template>

<!-- For anything more concrete, the following templates will be used -->
<xsl:template name="AddCell">
    <xsl:element name="table:table-cell">
        <xsl:attribute name="office:value-type">time</xsl:attribute>
        <xsl:attribute name="office:time-value">PT<xsl:value-of select="$time_h" />H<xsl:value-of select="$time_m" />M00S</xsl:attribute>
        <xsl:attribute name="calcext:value-type">time</xsl:attribute>
        <xsl:element name="text:p"><xsl:value-of select="$time_h" />:<xsl:value-of select="$time_m" /></xsl:element>
    </xsl:element>
</xsl:template>
    
<!--We have to check all table rows, since variables are not allowed in'match' -->
<xsl:template match="table:table-row">
    <xsl:element name="table:table-row">
        <xsl:choose>
            <xsl:when test="table:table-cell/@office:date-value=$dateString and table:table-cell/@calcext:value-type='date'">
                <xsl:for-each select="table:table-cell">
                    <xsl:choose>
		                <xsl:when test="(count(preceding-sibling::table:table-cell)=3) and $isArrival='TRUE'">
		                    <xsl:choose>
                                        <!-- Only add 'come' if not already there -->
		                        <xsl:when test="@office:value-type='time'">
		                            <xsl:element name="table:table-cell">
		                                <xsl:apply-templates select="@* | node()" />
		                            </xsl:element>
		                        </xsl:when>
		                        <xsl:otherwise>
		                            <xsl:call-template name="AddCell"/>
					    <!-- Add placeholder for leave -->
		                            <xsl:element name="table:table-cell">
		                                <xsl:attribute name="table:number-columns-repeated"><xsl:value-of select="1" /></xsl:attribute>
		                            </xsl:element>
		                        </xsl:otherwise>
		                    </xsl:choose>
		                </xsl:when>
		                <!-- Either we are on a placeholder, or we are on an already added entry -->
		                <xsl:when test="(count(preceding-sibling::table:table-cell)=4) and $isArrival='FALSE' and not(@table:formula)">
		                    <xsl:call-template name="AddCell"/>
		                </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="table:table-cell">
                                <xsl:apply-templates select="@* | node()" />
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise><!--$dateString not found - just copy stuff  -->
                <xsl:apply-templates select="@* | node()" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
