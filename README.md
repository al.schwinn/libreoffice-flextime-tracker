**Libreoffice Tabelle zur einfachen Verwaltung der kommen / gehen Zeiten für GSI/FAIR**

*Lies den Text in einer anderen Sprache: [English](README.en.md), [Deutsch](README.md)*

- einfach anpassbar
- Support für Teilzeitmodelle
- bash scripts zur automatischen Erfassung
- monatliche Zusammenfassung die man 1:1 in den GSI Zeiterfassungsbogen kopieren kann (Urlaub/Gleittage/etc. werden nicht explizit aufgeführt, das ist aber für PA kein Problem)

![screenshot](./static/screenshot.png)

**Schnellstartanleitung**

- Öffne den Reiter 'general'

![general](./static/general.png)

- Trage in A2 (First day of year) den ersten Tag des aktuellen Jahres ein 
- Trage in B4 (Credit form last year [h]) die Gleitzeit ein die aus dem letzen Jahr übernommen wird
- Trage in B5 (Vacation left from last year) den Urlaub ein, der aus dem letzen Jahr übernommen wird
- Trage in B6 (New vacation for this year) den neuen Urlaub für diese Jahr ein
- Trage in B10 (betrFZ for this year) die neue betriebliche Freizeit für dieses Jahr ein
- Optional: Bei Teilzeit, trage die entsprechenden Wochenstunden in B13-B17 ein

Fertig. Nun kannst du am ersten Arbeitstag im Januar deine erste Startzeit/Endzeit eintragen

**Scripts zum automtischen Eintragen der Zeiten**

Unter Linux können die scripts `on_login` und `on_logout` genutzt werden um die Arbeitszzeiten beim login/logout am computer automatisch die Arbeitszeit zu erfassen. (Mit optional +/- 5min extra). Wie man ein script bei login/logout ausführt hängt von der genutzen Desktop-Umgebung ab.

**Bewegliche Feiertage**

Bewegliche Feiertage muss man von Hand eintragen. Siehe auch: [Infos Heinerfest und Fastnachtsdienstag](https://www.gsi.de/work/administration/personalabteilung/arbeitszeitinformationen)
