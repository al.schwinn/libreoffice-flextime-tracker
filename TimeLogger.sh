#!/bin/bash
set -u
set -e

# Define command to leave this script
if [ ${BASH_SOURCE[0]} == $0 ]; then
        # the script is called from within another script. Means we have to use exit to leave
        LEAVE_TIME_LOGGER_SCRIPT=exit                                                         
else                                                                                         
        # the script is sourced (called from a console) means we need to use return (exit would close the console)
        LEAVE_TIME_LOGGER_SCRIPT=return                                                                            
fi                                                                                                                

function print_help
{
  	echo ""
  	echo "Script to modify a GSI Flextime-table libre-office file in order to automatically add arrival and departure time"
  	echo "Usually this script should be evoked on system start/shutdown or on user login/logout"
  	echo "-on double-arrival at the same day, the first value will kept"
  	echo "-on double-departure at the same day, the last value will kept"
  	echo ""
    echo "Use: TimeLogger.sh [File] [Option]... "
    echo "E.g: ./TimeLogger.sh /home/MyName/Time2015.ods -a 10"
    echo "--> Log arrival to specified file, with 10minutes transit-time"
	echo "-a log arrival time, transit-time is substracted"
	echo "-d log departure time, transit-time is added"
	echo "-h print this help text"
	echo ""
}

# Help is needed ?
if [ "$#" -lt 1  ];then
    print_help
	${LEAVE_TIME_LOGGER_SCRIPT} 0
fi  

# Help is needed ?
if [ "$1" == "-h"  ];then
    print_help
	${LEAVE_TIME_LOGGER_SCRIPT} 0
fi  

# Check Parameters
if [ $# -ne 3  ];then
	echo ""
    echo "Error: Wrong number of parameters!"
    print_help
	${LEAVE_TIME_LOGGER_SCRIPT} 1
fi                                                                                                                                                                   

FILE=$1
if [ "$2" == "-a"  ];then
    IS_ARRIVAL="TRUE"
else
    IS_ARRIVAL="FALSE"
fi
EXTRA_MINUTES=$3

TEMP_LOCATION_BASE=/tmp/TimeLogger
TEMP_LOCATION_DATA=${TEMP_LOCATION_BASE}/data

rm -rf ${TEMP_LOCATION_BASE}
mkdir ${TEMP_LOCATION_BASE}
mkdir ${TEMP_LOCATION_DATA}
unzip ${FILE} -d ${TEMP_LOCATION_DATA}

DATE=$(date +"%Y-%m-%d")

if [ "$IS_ARRIVAL" == "TRUE"  ];then
    TIME_H=$(date -d "-${EXTRA_MINUTES} minutes" +"%H")
    TIME_M=$(date -d "-${EXTRA_MINUTES} minutes" +"%M")
else
    TIME_H=$(date -d "+${EXTRA_MINUTES} minutes" +"%H")
    TIME_M=$(date -d "+${EXTRA_MINUTES} minutes" +"%M")
fi

echo "DATE: ${DATE}"
echo "IS_ARRIVAL: ${IS_ARRIVAL}"
echo "TIME_H: ${TIME_H}"
echo "TIME_M: ${TIME_M}"

xsltproc --noout --stringparam dateString "${DATE}" \
         --stringparam isArrival "${IS_ARRIVAL}" \
         --stringparam time_h "${TIME_H}" \
         --stringparam time_m "${TIME_M}" \
         -o "${TEMP_LOCATION_DATA}/content.xml" \
         /usr/local/bin/TimeLogger.xsl \
         "${TEMP_LOCATION_DATA}/content.xml" \
         
cd ${TEMP_LOCATION_DATA}
zip -0 -X ${FILE} mimetype
zip -r ${FILE} * -x mimetype



